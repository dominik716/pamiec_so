import java.util.*;



public class VirtualMemory
{
    static public Memory Ram = new Memory();
    public VirtualMemory()
    {
        setRamStatus();
    }

    static public Window window = new Window();

    class Process
    {
        int processId;
        Vector<Byte> code;
    }
    public void createProcess(int pid, Vector<Byte> exec)
    {
        Process p = new Process();
        p.processId = pid;
        p.code = exec;
        getProcess(p);
    }
    /**Method that was given by PCB*/
    void getProcess(Process p)
    {
        int maxPages = processProcessing(p);
        for(int i = 0; i <= maxPages; i++)
            putPageInRam(p.processId, i);
    }
    /**Method used by RAM to get a specific page*/
    public static int getFrame(int processID, int page)
    {
        Vector<PageEntry> test = PageTables.get(processID);
        if (test == null || !PageTables.get(processID).get(page).valid)
        {
            demandPage(processID, page);
        }
        return PageTables.get(processID).get(page).frame;
    }
    /**When page is missing in RAM, this method is used.*/
    static void demandPage(int ProcessID, int PageID)
    {
        putPageInRam(ProcessID, PageID);
        return;
    }
    /**Method that removes process from all containers when it's done.*/
    public void removeProcess(int pID)
    {
        Queue<Integer> tmpQueue = victimQueue, beginQ = new LinkedList<>();
        int frame, Qsize;
        for(int i = 0; i<16; i++)
        {
            if(RamStatus[i].ProcessID == pID)
            {
                Qsize = tmpQueue.size();
                /**Updating queue
                 * We are looking in queue for frame that process was occupying
                 */

                for(int j=0; j<Qsize; j++)
                {
                    frame = tmpQueue.poll();
                    if(frame != i)
                    {
                        /**If frame is different from the looking one it's added to the new "begin" queue*/
                        beginQ.add(frame);
                    } else break;
                }
                /**When the frame is found, the rest of records form queue is added to the tmp queue with the begging of the previous*/
                beginQ.addAll(tmpQueue);

                /**Removing all records from tmpQueue to prevent putting trash into the main Queue*/
                tmpQueue.removeAll(tmpQueue);
                /**Saving the queue without found frame-to-remove to the cleaned queue*/
                tmpQueue.addAll(beginQ);
                /**Removing all records from the begin Queue*/
                beginQ.removeAll(beginQ);
                RamStatus[i].ProcessID = -1;
                RamStatus[i].PageID = -1;
            }
        }
        //czyszczenie kolejki glownej
        //victimQueue.removeAll(victimQueue);
        //aktualizacja zawartosci kolejki glownej
        //victimQueue.addAll(tmpQueue);
        /**Removing all records from PageFile and PageTables maps*/
        PageFile.remove(pID);
        PageTables.remove(pID);
    }

    /**
     * Methods used for printing containers in shell
     * */

    public static void printPageTable (int processID)
    {
        if(processExists(processID))
        {
       window.log("#### Printing page table, process ID: " + processID + " ####");
            window.log("Page\tframe\tvalid");
        for(int i=0; i<PageTables.get(processID).size(); i++)
        {
            window.log(i + "   \t" + PageTables.get(processID).get(i).frame + "   \t" + PageTables.get(processID).get(i).valid);
        }
        } else window.log("Error: process with given ID doesn't exist.");
    }

    public static void printQueue()
    {
        Queue<Integer> tmp = new LinkedList<>(victimQueue);
        window.log("#### Printing victimQueue ####");
        int siz = tmp.size();
        for(int i=0; i<siz; i++)
        {
            window.log(tmp.poll() + " ");
        }
        window.log("");
    }


    public static void printProcessPages(int processID)
    {
        if(processExists(processID))
        {
        window.log("#### Printing process pages, process ID: " + processID + " ####");
        Vector<Vector<Byte>> pages = PageFile.get(processID);
        for(int i=0; i<pages.size(); i++)
        {
            window.log("Page no."+i);
            for(int j=0; j<pages.get(i).size(); j++)
            {
                window.log(String.format("%d", pages.get(i).get(j))+"\t");
            }
            window.log("");
        }
            window.log("");
        } else window.log("Error: process with given ID doesn't exist.");
    }

     /*
    public static void printPage(int processID, int pageID)
    {
        if(processExists(processID))
        {
            if(pageExists(processID, pageID))
            {
            window.log("#### Printing process page, process ID: " + processID + "\t pageID: " + pageID + " ####");
            for(int j=0; j<PageFile.get(processID).get(pageID).size(); j++)
                {
                 Shell.print(String.format("%d",PageFile.get(processID).get(pageID).get(j))+"\t");
                }
            window.log("");
            } else window.log("Error: page with given ID doesn't exist.");
        } else window.log("Error: process with given ID doesn't exist.");
    }
    */
     /*
    public static void printRamStatus()
    {
        window.log("#### Printing current RAM status ####");
        for(int i=0; i<16; i++)
        {
            //Utils.log("Frame ID: " + i + " PageID " + RamStatus[i].PageID + "\t ProcessID " + RamStatus[i].ProcessID);
            window.log("Frame ID: " + i + " PageID " + RamStatus[i].PageID + "\t ProcessID " + RamStatus[i].ProcessID);
        }
    }
      */
     /*
    public static void printNextVictim()
    {
        window.log("#### Printing next victim frame ####");
        window.log(String.format("%d",victimQueue.peek()));
    }
      */
    static boolean processExists(int procID){
        return PageFile.containsKey(procID);
    }

    static boolean pageExists(int procID, int pageID){

        try{
            PageTables.get(procID).get(pageID);
            //System.out.println(PageTables.get(procID).get(pageID));
            return true;
        }  catch (Exception ignored) {}
        return false;
    }

    private
            /**Single record in PageTable*/
    class PageEntry
    {
        boolean valid;
        int frame;
        PageEntry()
        {
            valid = false;
            frame = -1; //vjhvjhjvhjv //teshdfhkjkkj //jhfkdjhsbf
        }
    }

    /**Class that helps program to get info about what's inside of RAM*/
    class WhatsInside
    {
        int ProcessID;
        int PageID;
        WhatsInside()
        {
            this.PageID = -1;
            this.ProcessID = -1;
        }
    }

    /**Initialisation of PageFile.
     * Integer is processID
     * 2-dimensional vector contains program code*/
    static Map<Integer, Vector<Vector<Byte>>> PageFile = new HashMap<>();

    /**Map that contains all page tables that are: in use/RAM/not removed*/
    static Map<Integer, Vector<PageEntry>>  PageTables = new HashMap<>();

    /**Queue that contains the next victim to remove when RAM is full*/
    static Queue<Integer> victimQueue = new LinkedList<>();

    /**An array that contains information of what's in  atm*/
    static WhatsInside[] RamStatus = new WhatsInside[16];
    void setRamStatus()
    {
        for(int i=0; i<16; i++)
        {
            RamStatus[i] = new WhatsInside();
        }
    }

    /**Method that is processing process. It prepares the process to use it, put in PageFile, create PageTable and send it to RAM*/
    int processProcessing(Process proc)
    {

        Vector<Vector<Byte>> program = new Vector<>(new Vector<>());
        Vector<PageEntry> pageTable = new Vector<>();
        int maxPageID = proc.code.size() / 16;
        for (int currentPageID = 0; currentPageID <= maxPageID; currentPageID++)
        {
            Vector<Byte> page = new Vector<>();
            pageTable.add(new PageEntry());

            for (int j = 0; j < 16; j++)
            {
                try {
                    page.add(proc.code.get(j + currentPageID * 16));
                }
                catch (Exception ignored) {}
            }
            program.add(page);
        }
        putInfoToPageTable(proc.processId, pageTable);
        putProcessToPageFile (proc.processId, program);
        return maxPageID;
    }

    /**Method that puts certain page in RAM*/
    static void putPageInRam(int procID, int pageID)
    {
        Vector <Byte> Page = PageFile.get(procID).get(pageID);
        /**Checking if RAM is full*/
        if(victimQueue.size()<16)
        {
            for(int fID = 0; fID < 16; fID++)
            {
                if(RamStatus[fID].ProcessID == -1)
                {
                    if(putPageIn(fID, Page))
                    {
                        /**If sent correctly modify all tables connected with status and pages
                         * Updating pageTable and ramStatus*/
                        updatePageTables(procID, pageID, fID, true);
                        updateRamStatus(procID, pageID, fID);
                        victimQueue.add(fID);
                        break;
                    }
                }
            }
        }
        /**That means ram is full, there is need to kill the victim*/
        else
        {
            int fID = findVictim();
            takePageOut(fID);
            if(putPageIn(fID, Page))
            {
                /**If sent correctly modify all tables connected with status and pages
                 * Updating pageTable and ramStatus*/
                updatePageTables(procID, pageID, fID, true);
                updateRamStatus(procID, pageID, fID);
                victimQueue.add(fID);
            }
        }
    }

    /**Method that removes certain page from RAM*/
    static void takePageOut(int fID)
    {
        Vector <Byte> page;
        page = Ram.readFrame(fID);
        int prID = RamStatus[fID].ProcessID;
        int pgID = RamStatus[fID].PageID;
        putPageInPageFile(pgID, prID, page);
        updatePageTables(prID, pgID, -1, false);
        updateRamStatus(-1, -1, fID);
    }

    /**Method that puts certain page from pageFile into RAM*/
    static boolean putPageIn(int FrameID, Vector<Byte> Page)
    {
        return Ram.write(Page, FrameID);
    }

    /**Methods that update PageTables and RamStatus with specific changes*/
    static void updatePageTables(int procID, int pageID, int frameID, boolean value)
    {
        PageTables.get(procID).get(pageID).frame = frameID;
        PageTables.get(procID).get(pageID).valid = value;
    }
    static void updateRamStatus(int procID, int pageID, int fID)
    {
        RamStatus[fID].ProcessID = procID;
        RamStatus[fID].PageID = pageID;
    }

    void putProcessToPageFile(int pID, Vector<Vector<Byte>> pr)
    {
        PageFile.put(pID, pr);
    }
    static void putPageInPageFile(int pageID, int procID, Vector<Byte> page)
    {
        Vector<Vector<Byte>> tmp =  PageFile.get(procID);
        tmp.set(pageID, page);
        PageFile.put(procID, tmp);
    }
    void putInfoToPageTable(int pID, Vector<PageEntry> pT)
    {
        //window.log("Putting info into PageTable, processID: " + pID);
        PageTables.put(pID, pT);
    }
    static int findVictim()
    {
        return victimQueue.poll();
    }

    public static void updateVirtualMemoryWindow()
    {
        window.logln("Zawartosc tablicy stron:");
        PageTables.forEach((k, v) -> {
            int pageNumber = 0;
            window.log("Proces o numerze: " + k + "");
            for(PageEntry page : v)
            {
                window.log("Strona numer " + pageNumber++ + ": ");
                window.log("Numer ramki: " + page.frame + " ");
                window.log("Bit poprawnosci: " + page.valid);
                window.log("\n");
            }
        });

        window.logln("Zawartosc kolejki stron do usuniecia z pamieci gdy RAM bedzie pelny:");
        for(int number : victimQueue){
            window.log(number + " ");
        }
        window.logln("");
    }

}