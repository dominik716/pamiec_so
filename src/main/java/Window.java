import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


    /**
     * Utility methods used across all classes
     */
    public class Window {
        /**
         * If set to true, logs are printed
         */
         private boolean logging = true;
         private boolean stepping = false;

        private  final JFrame frame = new JFrame();
        private  final JTextArea textArea = new JTextArea(50, 10);
        private  final String KEY = "ENTER";
        private  final JButton enterButton = new JButton(KEY);
        private  final PrintStream printStream = new PrintStream(new CustomOutputStream(textArea));
        private  final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        private  String lastLogTime = "00:00:00";


        Window() {
            frame.add( new JScrollPane( textArea )  );
            frame.pack();
            frame.setVisible( true );
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            textArea.setFont(new Font("Lucida Console", Font.PLAIN,18));
            textArea.setEditable( false );
            frame.setSize(800,600);
            frame.setAlwaysOnTop(true  );

            KeyStroke keyStroke = KeyStroke.getKeyStroke(KEY);
            Object actionKey = textArea.getInputMap(
                    JComponent.WHEN_FOCUSED).get(keyStroke);


        }

        /**
         * Closes second window
         */
        public  void closeLogs() { frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)); }




        public void log(String msg) {
            printStream.print( msg );
        }

        public  void logln(String msg) {
            printStream.println( msg );
        }

        public  void clearWindow()
        {
            textArea.setText("");
        }




}
