import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Test");
        Memory.updateRamWindow();
        Window window = new Window();
        window.log("Test");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
        Vector<Byte> data = new Vector<Byte>();
        data.add((byte)3);
        data.add((byte)5);

        Memory.write(data, 1);
        Memory.updateRamWindow();
        window.clearWindow();
        br.readLine();
    }

}
